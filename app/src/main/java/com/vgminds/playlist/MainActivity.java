package com.vgminds.playlist;

import android.content.res.TypedArray;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.io.File;

public class MainActivity extends FragmentActivity implements AdListener {
    public static MainActivity getInstance() { return instance; }
    static MainActivity instance;
    public final static String TAG = "playlist";

    Ads ads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        switch(keycode) {
            case KeyEvent.KEYCODE_I:
                init();
                break;
            case KeyEvent.KEYCODE_S:
                if(ads == null) ads = new Ads(this);
                ads.show();
                break;
        }
        return super.onKeyDown(keycode, event);
    }

    void init() {
        ads = new Ads(this);
    }

    @Override
    public void onAdClosed() {
        Log.d(TAG, "onAdClosed: ");
    }

    @Override
    public void onAdFailedToLoad() {
        Log.d(TAG, "onAdFailedToLoad: ");
    }
}
