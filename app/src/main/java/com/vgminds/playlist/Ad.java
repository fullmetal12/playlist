package com.vgminds.playlist;

public interface Ad {
    boolean isReady();
    void show();
}
