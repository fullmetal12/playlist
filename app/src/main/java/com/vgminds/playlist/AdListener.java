package com.vgminds.playlist;

public interface AdListener {
    void onAdClosed();
    void onAdFailedToLoad();
}
