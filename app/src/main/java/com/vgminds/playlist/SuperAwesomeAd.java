package com.vgminds.playlist;

import android.util.Log;
import tv.superawesome.sdk.publisher.*;

import static com.vgminds.playlist.MainActivity.TAG;

public class SuperAwesomeAd implements Ad {
    MainActivity mainActivity;
    AdListener adListener;
    int ad_id = 30473;

    public SuperAwesomeAd(MainActivity mainActivity, final AdListener adListener) {
        Log.d(TAG, "SuperAwesomeAd: initializing super awesome ad");
        Log.d(TAG, "SuperAwesomeAd: application is " + (MainActivity.instance.getApplication() == null ? "null." : "not null."));
        AwesomeAds.init(MainActivity.instance.getApplication(), true);
        SAInterstitialAd.enableTestMode ();
        SAInterstitialAd.setConfigurationProduction ();
        SAInterstitialAd.setOrientationLandscape ();
        SAInterstitialAd.enableBackButton ();
        SAInterstitialAd.setListener(new SAInterface() {
            @Override
            public void onEvent(int i, SAEvent saEvent) {
                Log.d(TAG, "SA::onEvent: " + saEvent);
                switch (saEvent){
                    case adClosed:
                        adListener.onAdClosed();
                        break;
                    case adFailedToShow:
                        Log.d(TAG, "SA::onEvent: Ad failed to show");
                        adListener.onAdFailedToLoad();
                        break;
                }
            }
        });
        SAInterstitialAd.load (ad_id, mainActivity);
        Log.d(TAG, "onCreate: SuperAwesomeAd created.");
    }

    @Override
    public boolean isReady() {
        return SAInterstitialAd.hasAdAvailable(ad_id);
    }

    @Override
    public void show() {
        Log.d(TAG, "SA::show: trying to show ads");
        if(isReady()) {
            Log.d(TAG, "SA::show: if");
            SAInterstitialAd.play(ad_id, mainActivity);
        } else {
            Log.d(TAG, "SA::show: else");
            adListener.onAdFailedToLoad();
        }
    }
}
