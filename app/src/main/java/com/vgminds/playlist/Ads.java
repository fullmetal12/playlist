package com.vgminds.playlist;

import android.content.Context;
import android.util.Log;

import static com.vgminds.playlist.MainActivity.TAG;

public class Ads implements Ad, AdListener {

    Ad[] ads;
    int readyAdIndex = -1;
    AdListener adListener;
    MainActivity mainActivity;
    boolean _initialized = false;

    public Ads(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    void Init() {
        adListener = mainActivity;
        ads = new Ad[1];
        ads[0] = new SuperAwesomeAd(mainActivity, this);
        Log.d(TAG, "ADS::Init: initialized");
        _initialized = true;
    }

    @Override
    public boolean isReady() {
        if(!_initialized) Init();
        Log.d(TAG, "Ads.java::isReady::ads is null: "+ (ads == null) + ", length: " + ads.length);
        for(int index = 0; index < ads.length; index++) {
            Log.d(TAG, "Ads::isReady::ads[" + index + "]: " + (ads[index] == null ? "null" : "not null"));
            if(ads[index].isReady()) {
                readyAdIndex = index;
                return true;
            } else {
                Log.d(TAG, "Ads::isReady: " + index + " is not ready");
            }
        }
        readyAdIndex = -1;
        return false;
    }

    @Override
    public void show() {
        if(!_initialized) Init();
        Log.d(TAG, "Ads::show: tyring to show ads..");
        if(readyAdIndex >= 0 || isReady()) {
            ads[readyAdIndex].show();
            readyAdIndex = -1;
        }
    }

    @Override
    public void onAdClosed() {
        adListener.onAdClosed();
    }

    @Override
    public void onAdFailedToLoad() {
        adListener.onAdFailedToLoad();
    }
}
